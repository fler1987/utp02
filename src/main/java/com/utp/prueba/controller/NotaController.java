package com.utp.prueba.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.utp.prueba.model.Nota;
import com.utp.prueba.services.NotaService;

@RestController
@RequestMapping("/nota")
public class NotaController {

	@Autowired
	NotaService notaService;
	
		
	@GetMapping()
	public ArrayList<Nota> listarNotas(){
		return notaService.listarNotas();
	}
	
	@PostMapping()
	public Nota crearNota(@Valid @RequestBody Nota nota) {		
		return notaService.crearNota(nota);
	}	
		
}
