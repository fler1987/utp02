package com.utp.prueba.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utp.prueba.model.Nota;
import com.utp.prueba.repo.NotaRepo;

@Service
public class NotaService {

	@Autowired
	NotaRepo notaRepo;
	
	public ArrayList<Nota> listarNotas(){
		return (ArrayList<Nota>)notaRepo.findAll();
	}
	
	public Nota crearNota(Nota nota) {
		return notaRepo.save(nota);
	}
	
//	public Nota obtenerNota(String usuario) {
//		return notaRepo.findById(id) AllById(ids) ()
//	}
}
